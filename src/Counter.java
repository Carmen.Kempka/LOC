
public class Counter {

	int n;
	public Counter() {
		this.n=0;
	}
	
	public void increase(){
		n++;
	}
	
	public int getN(){
		return n;
	}
	
}
