public class A2 {

	private enum State {
		codeFound, 
		noCodeFound,
	};

	private State currentState;
	Runnable onEndOfLineWithCode;

	
	public A2(Runnable runnable) {
		currentState = State.noCodeFound;
		this.onEndOfLineWithCode = runnable;
	}

	public void onEOL() {
		if (currentState == State.codeFound) {
			onEndOfLineWithCode.run();
		}
		currentState = State.noCodeFound;
	}

	public void onCodeFound() {
		currentState = State.codeFound;
	}

}
