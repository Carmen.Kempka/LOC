import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LinesOfCodeTest {

	@Test
	public void locTest() {
		LinesOfCode linesOfCode = new LinesOfCode();
		String sourceCode = 
				"int i = 1;" + "\n"  //code
				+ "//comment" + "\n" //kein code
				+ "x++;" + "\n"     //code
				+ "int y /* comment */ = 42;" + "\n" //code 
				+"/* comment" + "\n"  //kein code
				+ "int x = 5;" + "\n" //kein code
				+ " mehr comment */ x = 0;" + "\n" //code 
				+	" " + "\n"  //kein code
				+ "\t\t" + "\n" //kein code
				+ "/*/ ganz fieser kommentar /*/" + "\n" //kein code 
				+ "/***/ int j = 666; // Ich bin kein code" + "\n" //code
				+ "string /* bla */ x = \"/* nbo bla\"/* comment */" + "\n" //code
				+ "// string x = \"mystring\";" + "\n" //kein code
				+ "/* /* Kommentar? */"	+ "\n" //kein code
				+ "int x = 5" + "\n" //code
				+ "/" + "\n" //code
				+ "2;"; //code

		int loc = linesOfCode.LOC(sourceCode);
		assertEquals(9, loc);
	}
	
}
