import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Consumer;

public class A1 {

	private enum State {
		Code, potentialStartOfComment, comment, potentialEndOfComment, commentUntilEOL, Whitespace, inString, escapedString,
	};

	private State currentState;
	Runnable onCodeFound;
	
	public A1(Runnable onCodeFound) {
		currentState = State.Whitespace;
		this.onCodeFound = onCodeFound;
	}

	public void onChar(char character) {
		nextState(character);
	}

	private void nextState(char character) {
		switch (currentState) {
		case Code:
			fromCode(character);
			break;
		case potentialStartOfComment:
			fromPotentialStartOfComment(character);
			break;
		case comment:
			fromComment(character);
			break;
		case potentialEndOfComment:
			fromPotentialEndOfComment(character);
			break;
		case commentUntilEOL:
			fromCommentUntilEOL(character);
			break;
		case Whitespace:
			fromWhitespace(character);
			break;
		case inString:
			fromInString(character);
			break;
		case escapedString:
			fromEscapedString(character);
			break;
		}
	}

	private void fromCode(char character) {

		if (Character.isWhitespace(character)) {
			return;
		}

		switch (character) {
		case '/':
			currentState = State.potentialStartOfComment;
			break;

		case '"':
			currentState = State.inString;
			break;

		case '\n':
			currentState = State.Whitespace;
			break;

		default:
			currentState = State.Code; 
			onCodeFound.run();
			break;
		}

	}

	private void fromPotentialStartOfComment(char character) {

		switch (character) {
		case '*':
			currentState = State.comment;
			break;
			
		case '/':
			currentState = State.commentUntilEOL;
			break;

		case '\n':
			currentState = State.Whitespace;
			onCodeFound.run();
			break;

		default:
			currentState = State.Code;
			onCodeFound.run();
			break;
		}
	}

	private void fromComment(char character) {
		switch (character) {
		case '*':
			currentState = State.potentialEndOfComment;
			break;

		default:
			break;
		}

	}

	private void fromPotentialEndOfComment(char character) {
		switch (character) {
		case '/':
			currentState = State.Whitespace;
			break;

		case '*':
			break;

		default:
			currentState = State.comment;
			break;
		}
	}

	private void fromCommentUntilEOL(char character) {
		switch (character) {
		case '\n':
			currentState = State.Whitespace;
			break;

		default:
			break;
		}

	}

	private void fromWhitespace(char character) {
		fromCode(character);
	}

	private void fromInString(char character) {
		switch (character) {
		case '\\':
			currentState = State.escapedString;
			break;

		case '"':
			currentState = State.Code;
			onCodeFound.run();
			break;

		default:
			break;
		}
	}

	private void fromEscapedString(char character) {
		currentState = State.inString;
	}

}
