import static org.junit.Assert.*;

import org.junit.Test;

public class A2Test {

	String indicator;

	@Test
	public void testA2() {
		String lineEndNote = "Zeilenende einer Zeile mit Code";

		A2 a2 = new A2(() -> {
			setIndicator(lineEndNote);
		});
		a2.onEOL();
		assertNotEquals(lineEndNote, indicator);
		a2.onCodeFound();
		a2.onEOL(); // hier wird hoffentlich run() aufgerufen
		assertEquals(lineEndNote, indicator);
		resetIndicator();
		a2.onEOL();
		assertNotEquals(lineEndNote, indicator);
	}

	private void setIndicator(String indicator) {
		this.indicator = indicator;
	}

	private void resetIndicator() {
		setIndicator("");
	}

}
