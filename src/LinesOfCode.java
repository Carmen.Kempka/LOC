
public class LinesOfCode {

	public int LOC(String code) {

		Counter counter = new Counter();
		A2 a2 = new A2(() -> counter.increase());
		A1 a1 = new A1(() -> a2.onCodeFound());
		for (int i = 0; i < code.length(); i++){
			
			char charI = code.charAt(i);
			a1.onChar(charI);
			if(charI == '\n'){
				a2.onEOL();
			}
		}
		a2.onEOL();
		return counter.getN();
	}

}
