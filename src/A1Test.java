import static org.junit.Assert.*;

import org.junit.Test;

public class A1Test {

	String indicator;
	
	
	
	@Test
	public void testA1(){
		String codeIndicator = "Code found";
		A1 a1 = new A1(() -> setIndicator(codeIndicator)); 
		
		resetIndicator();
		a1.onChar('x'); // code
		assertEquals(codeIndicator, indicator);
		
		resetIndicator();
		a1.onChar('/'); 
		a1.onChar('3'); // code
		assertEquals(codeIndicator, indicator);
		
		resetIndicator();
		a1.onChar(' ');
		a1.onChar('3'); // code
		assertEquals(codeIndicator, indicator);
	}
	
	private void setIndicator(String indicator) {
		this.indicator = indicator;
	}
	
	private void resetIndicator(){
		setIndicator("");
	}
	
}
